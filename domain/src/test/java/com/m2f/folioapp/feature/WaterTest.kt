package com.m2f.folioapp.feature

import com.m2f.folioapp.feature.bath.Water
import org.junit.After
import org.junit.Before
import org.junit.Test
import rx.Observable
import rx.observers.TestSubscriber
import java.util.concurrent.TimeUnit

/**
 * @author Marc Moreno
 * *
 * @version 1
 */
class WaterTest {

    lateinit var coldWaterFlow: Observable<Water>

    lateinit var hotWaterFlow:  Observable<Water>

    lateinit var subscriber: TestSubscriber<Water>

    @Before
    fun setUp() {
        coldWaterFlow = Observable.defer { Observable.interval(1, TimeUnit.SECONDS)
                .map { Water(10f, 12f) }
                .take(3)}

        hotWaterFlow = Observable.defer { Observable.interval(1, TimeUnit.SECONDS)
                .map { Water(50f, 10f) }}

        subscriber = TestSubscriber()
    }

    @After
    fun tearDown() {
        if (subscriber.isUnsubscribed.not()) {
            subscriber.unsubscribe()
        }
    }

    @Test
    fun testWaterFlow() {

        val add = { currentWater: Water, addedWater: Water ->
            val temp: Float = (currentWater.temeperature * currentWater.volume + addedWater.temeperature * addedWater.volume) / (currentWater.volume + addedWater.volume)
            val volume: Float = currentWater.volume + addedWater.volume
            Water(temp, volume)
        }

        val midWaterFlow = Observable.merge(coldWaterFlow, hotWaterFlow)
                .scan(add)
                .takeUntil { water -> water.volume >= 150 }

        midWaterFlow.subscribe(subscriber)

        subscriber.awaitTerminalEvent()

        subscriber.assertNoErrors()

        val onNextEvents = subscriber.onNextEvents

        for (onNextEvent in onNextEvents) {
            println("current temperature is ${onNextEvent.temeperature} with a total of ${onNextEvent.volume} litres")
        }
        assert(onNextEvents.isEmpty().not())


    }
}