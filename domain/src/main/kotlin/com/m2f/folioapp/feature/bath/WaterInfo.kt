package com.m2f.folioapp.feature.bath

/**
 * @author Marc Moreno
 * @version 1
 */
class WaterInfo(val hotWaterTemperature: Float,
                val coldWaterTemperature: Float,
                val hotWaterLitersPerMinute: Float,
                val coldWaterLitersPerMinute: Float)