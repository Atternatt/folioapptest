package com.m2f.folioapp.feature.bath

import java.util.concurrent.TimeUnit

/**
 * @author Marc Moreno
 * @version 1
 */
class WaterFactory(val waterInfo: WaterInfo) {

    fun coldWater(frequency:TimeUnit): Water {
        return Water(waterInfo.coldWaterTemperature, waterInfo.coldWaterLitersPerMinute / frequency.convert(1, TimeUnit.MINUTES))
    }

    fun hotWater(frequency:TimeUnit): Water {
        return Water(waterInfo.hotWaterTemperature, waterInfo.hotWaterLitersPerMinute / frequency.convert(1, TimeUnit.MINUTES))
    }
}