package com.m2f.folioapp.feature.bath

/**
 * @author Marc Moreno
 * @version 1
 */
enum class WaterStatus {
    colder,
    cold,
    mid,
    warm,
    hot,
    unknown


}