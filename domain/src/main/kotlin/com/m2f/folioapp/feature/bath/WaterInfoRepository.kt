package com.m2f.folioapp.feature.bath

import rx.Single

/**
 * @author Marc Moreno
 * @version 1
 */
interface WaterInfoRepository {

    fun getWaterInfo(): Single<WaterInfo>
}