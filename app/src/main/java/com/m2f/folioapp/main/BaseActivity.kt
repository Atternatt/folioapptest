package com.m2f.folioapp.main

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.os.Bundle
import android.support.annotation.LayoutRes
import android.support.v7.app.AppCompatActivity
import com.m2f.folioapp.di.ApplicationComponent
import com.m2f.folioapp.di.ComponentReflectionInjector

abstract class BaseActivity<out T : Any> : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val component = component
        val injector = ComponentReflectionInjector(component.javaClass, component)
        injector.inject(this)
    }

    val applicationComponent: ApplicationComponent
        get() = (getApplication() as AndmeApplication).getApplicationComponent()

    abstract val component: T

    @LayoutRes
    abstract fun layout(): Int

    fun <B : ViewDataBinding> generateBinding(): B {
        return DataBindingUtil.setContentView<B>(this, layout())
    }


}

