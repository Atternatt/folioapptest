package com.m2f.folioapp.main

import android.app.Application
import com.m2f.folioapp.di.ApplicationComponent
import com.m2f.folioapp.di.ApplicationModule
import com.m2f.folioapp.di.DaggerApplicationComponent

/**
 * @author Marc Moreno
 * @version 1
 */
class AndmeApplication : Application() {

    override fun onCreate() {
        super.onCreate()
    }

    fun getApplicationComponent() : ApplicationComponent {
        return DaggerApplicationComponent.builder()
                .applicationModule(ApplicationModule(this))
                .build()
    }

}