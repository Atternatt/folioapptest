package com.m2f.folioapp.features.bath

import android.databinding.BindingAdapter
import android.support.v4.content.ContextCompat
import android.widget.TextView
import com.m2f.folioapp.R
import com.m2f.folioapp.feature.bath.WaterStatus

/**
 * @author Marc Moreno
 * @version 1
 *
 * this contain the methods that will bind custom properties in the xml file
 */

/**
 * This can be translated as:
 * if we found a TextView with app:temperatureproperty with a WaterStatus parameter then
 * we launch this method that sets the text color of te textWied dependinc de WaterStatus (that is an enum)
 */
@BindingAdapter("app:temperature")
fun setWAterStatus(textView: TextView, waterStatus: WaterStatus?) {

    if (null != waterStatus) {

        when (waterStatus) {
            WaterStatus.colder -> textView.setTextColor(ContextCompat.getColor(textView.context, R.color.colderWater))
            WaterStatus.cold -> textView.setTextColor(ContextCompat.getColor(textView.context, R.color.coldWater))
            WaterStatus.mid -> textView.setTextColor(ContextCompat.getColor(textView.context, R.color.midWater))
            WaterStatus.warm -> textView.setTextColor(ContextCompat.getColor(textView.context, R.color.warmWater))
            WaterStatus.hot -> textView.setTextColor(ContextCompat.getColor(textView.context, R.color.hotWater))
        }


    }
}
