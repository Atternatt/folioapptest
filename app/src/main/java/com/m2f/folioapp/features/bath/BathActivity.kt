package com.m2f.folioapp.features.bath

import android.os.Bundle
import com.jakewharton.rxbinding.widget.RxCompoundButton
import com.m2f.folioapp.R
import com.m2f.folioapp.databinding.ActivityMainBinding
import com.m2f.folioapp.main.BaseActivity
import javax.inject.Inject


/**
 * @author Marc Moreno
 * @version 1
 *
 * This is the Activity that show the core content of the application (see test question)
 */
class BathActivity : BaseActivity<BathComponent>() {

    @Inject
    lateinit var viewModel: BathViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val binding: ActivityMainBinding = generateBinding()

        val coldWaterTap = RxCompoundButton.checkedChanges(binding.coldWaterSwitch)
        val hotWaterTap = RxCompoundButton.checkedChanges(binding.hotWaterSwitch)

        viewModel.setTaps(coldWaterTap, hotWaterTap)
        binding.bath = viewModel
    }

    //region BaseActivity implementation
    override val component: BathComponent
        get() = DaggerBathComponent.builder()
                .applicationComponent(applicationComponent)
                .bathModule(BathModule())
                .build()

    override fun layout(): Int {
        return R.layout.activity_main
    }
    //endregion

}
