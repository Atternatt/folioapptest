package com.m2f.folioapp.features.bath

import com.m2f.folioapp.di.ActivityScope
import com.m2f.folioapp.di.ApplicationComponent
import dagger.Component

/**
 * @author Marc Moreno
 * @version 1
 *
 * The component that injects in BathActivity's dependencies
 */
@ActivityScope
@Component(dependencies = arrayOf(ApplicationComponent::class), modules = arrayOf(BathModule::class))
interface BathComponent {

    fun inject(activity: BathActivity)

}