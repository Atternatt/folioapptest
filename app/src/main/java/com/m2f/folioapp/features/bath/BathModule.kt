package com.m2f.folioapp.features.bath

import com.m2f.folioapp.data.feature.bath.Api
import com.m2f.folioapp.data.feature.bath.ApiImpl
import com.m2f.folioapp.data.feature.bath.WaterInfoRepositoryImpl
import com.m2f.folioapp.di.ActivityScope
import com.m2f.folioapp.feature.bath.WaterInfoRepository
import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

/**
 * @author Marc Moreno
 * @version 1
 *
 * the object that provides dependencies injected by BathComponent to BathActivity
 */
@Module
class BathModule {

    @Provides
    @ActivityScope
    fun providesApi(retrofit:Retrofit):Api {
        return ApiImpl(retrofit)
    }

    @Provides
    @ActivityScope
    fun providesWaterInfoRepository(api: Api): WaterInfoRepository {
        return WaterInfoRepositoryImpl(api)
    }

    @Provides
    @ActivityScope
    fun providesViewModel(waterInfoRepository: WaterInfoRepository): BathViewModel {
        return BathViewModelImpl(waterInfoRepository)
    }
}