package com.m2f.folioapp.features.bath

import android.databinding.ObservableField
import com.m2f.folioapp.feature.bath.WaterStatus
import rx.Observable

/**
 * @author Marc Moreno
 * @version 1
 *
 * the Object that handels busines logic and updates the view via ObservableFiedls
 */
interface BathViewModel {

    /**
     * these are the objects that are binded in the xml and handle changes to update the view
     * automatically
     */
    val temperature: ObservableField<String>
    val volume: ObservableField<String>
    val litres: ObservableField<String>
    val waterStatus: ObservableField<WaterStatus>
    val canStart: ObservableField<Boolean>
    val coldWaterVolume: ObservableField<String>
    val hotWaterVolume: ObservableField<String>

    /**
     * A method tho bind view flows to the ViewModel
     */
    fun setTaps(coldWaterTap: Observable<Boolean>, hotWaterTap: Observable<Boolean>)

}