package com.m2f.folioapp.features.bath

import android.databinding.ObservableField
import android.util.Log
import com.m2f.folioapp.feature.bath.*
import rx.Observable
import rx.android.schedulers.AndroidSchedulers
import rx.lang.kotlin.subscribeWith
import java.util.concurrent.TimeUnit

/**
 * @author Marc Moreno
 * @version 1
 */
class BathViewModelImpl(val waterInfoRepository: WaterInfoRepository) : BathViewModel {

    companion object {
        private const val TEMP_FRACTIONS = 5
    }

    lateinit var midWater: Observable<Water>
    lateinit var waterFactory: WaterFactory
    lateinit var waterInfo: WaterInfo

    //ObservableField is a DataBinding library object that's used to observe changes and update the view.
    override val temperature: ObservableField<String> = ObservableField()
    override val volume: ObservableField<String> = ObservableField()
    override val litres: ObservableField<String> = ObservableField()
    override val waterStatus: ObservableField<WaterStatus> = ObservableField()
    override val canStart: ObservableField<Boolean> = ObservableField()
    override val coldWaterVolume: ObservableField<String> = ObservableField()
    override val hotWaterVolume: ObservableField<String> = ObservableField()

    init {
        canStart.set(false)
        coldWaterVolume.set("0%")
        hotWaterVolume.set("0%")
        volume.set("0%")
        temperature.set("0º")
    }

    /* Considering the following: Tgained = Tlost we can generate the following
     * equation:
     *
     * Vh * (Th - x) * (4.184) = Vc * (x - Tc) * (4.184)
     *
     * where Vh = Volume of the hotter liquid
     *       Th = Temperature of the hotter liquid
     *       Vc = Temperature of the colder liquid
     *       Tc = Temperature of the colder liquid
     *       x  = the temperature result of mixing both liquids
     *       4.184 = The energy needed to increase the temperature of a given mass of water by 1 °C (so we can extract it as we have water in both sides)
     *
     * If we plot this equation to be more easy to understand:
     *
     * Tc__________x__________Th  -> so (Th - x) AND (x - Tc) are the distances from the new temperature resulting by mixing both liquids
     *
     * We can isolate x from the equation and we obtain this new:
     *
     * x = (Th * Vh + Tc * Vc) / Vh + Lh
     */

    val addWaterFunction = { currentWater: Water, addedWater: Water ->
        val temp: Float = (currentWater.temeperature * currentWater.volume + addedWater.temeperature * addedWater.volume) / (currentWater.volume + addedWater.volume)
        val volume = currentWater.volume + addedWater.volume
        Water(temp, volume)
    }

    /**
     * Transforms a flow of booleans (open/closed tap) to a stream of water
     */
    fun flowWater(tapIsOpen: Boolean, waterGenerator: (TimeUnit) -> Water): Observable<Water> {
        if (tapIsOpen) {
            return Observable.interval(1, TimeUnit.SECONDS)
                    .map { waterGenerator.invoke(TimeUnit.SECONDS) }
        } else {
            return Observable.never()
        }

    }

    /**
     * this returns a WaterStatus depending the temperature of the water calculated by fractioning
     * the range of temperature in 5 sections.
     */
    fun calculateWaterStatus(temperature: Float): WaterStatus {
        val coldWaterTemperature = waterInfo.coldWaterTemperature.toInt()
        val fraction = ((waterInfo.hotWaterTemperature - coldWaterTemperature) / TEMP_FRACTIONS).toInt()
        val temp = temperature.toInt()
        val status = when (temp) {
            in coldWaterTemperature..coldWaterTemperature + fraction -> WaterStatus.colder
            in coldWaterTemperature + fraction..coldWaterTemperature + fraction * 2 -> WaterStatus.cold
            in coldWaterTemperature + fraction * 2..coldWaterTemperature + fraction * 3 -> WaterStatus.mid
            in coldWaterTemperature + fraction * 3..coldWaterTemperature + fraction * 4 -> WaterStatus.warm
            in coldWaterTemperature + fraction * 4..coldWaterTemperature + fraction * 5 -> WaterStatus.hot
            else -> WaterStatus.unknown
        }

        return status
    }

    /**
     * this binds 2 Observables of booleans (streams of open/closed taps) and calls flowWater to
     * transform it in flows of water. it previowsly get WaterInfo from the repository that is
     * used to know the cold and hot water temperature and the corresponding Litres per minute of
     * each one.
     */
    override fun setTaps(coldWaterTap: Observable<Boolean>, hotWaterTap: Observable<Boolean>) {

        waterInfoRepository.getWaterInfo()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith {
                    onSuccess { newWaterInfo ->

                        waterInfo = newWaterInfo
                        waterFactory = WaterFactory(waterInfo)

                        canStart.set(true)

                        val coldWaterGenerator: (TimeUnit) -> Water = { waterFactory.coldWater(it) }
                        val hotWaterGenerator: (TimeUnit) -> Water = { waterFactory.hotWater(it) }

                        val coldWaterFlow = coldWaterTap.switchMap { isOpen -> flowWater(isOpen, coldWaterGenerator) }.share()
                        val hotWaterFlow = hotWaterTap.switchMap { isOpen -> flowWater(isOpen, hotWaterGenerator) }.share()

                        coldWaterFlow.scan(addWaterFunction)
                                .subscribeWith {
                                    onNext { water -> coldWaterVolume.set(String.format("%d%%", (water.volume / 150 * 100).toInt())) }
                                    onError { error -> Log.e("ERROR", error.message) }
                                }

                        hotWaterFlow.scan(addWaterFunction)
                                .subscribeWith {
                                    onNext { water -> hotWaterVolume.set(String.format("%d%%", (water.volume / 150 * 100).toInt())) }
                                    onError { error -> Log.e("ERROR", error.message) }
                                }

                        midWater = Observable.merge(coldWaterFlow, hotWaterFlow)
                        midWater.scan(addWaterFunction)
                                .takeUntil { water -> water.volume >= 150 }
                                .subscribeWith {
                                    onNext { water ->
                                        temperature.set(String.format("%.1fº", water.temeperature))
                                        volume.set(String.format("%d%%", (water.volume / 150 * 100).toInt()))
                                        waterStatus.set(calculateWaterStatus(water.temeperature))
                                        litres.set(String.format("%.2f/150L", water.volume))
                                    }
                                    onError { error -> Log.e("ERROR", error.message) }
                                }
                    }
                }


    }
}


