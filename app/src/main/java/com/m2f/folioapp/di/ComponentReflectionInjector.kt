package com.m2f.folioapp.di

import java.lang.reflect.Method
import java.util.HashMap
import java.util.concurrent.ConcurrentHashMap

/**
 * @author Marc Moreno
 * @version 1
 *
 * This injector implementatios uses reflection to find injection methods with the correct injectecd target
 * */
class ComponentReflectionInjector<T>(private val componentClass: Class<T>, val component: T) : Injector {
    private val methods: HashMap<Class<*>, Method>?

    init {
        this.methods = getMethods(componentClass)
    }

    override fun inject(target: Any) {

        if (methods == null) {
            throw RuntimeException(
                    String.format("%s has no methods", componentClass.javaClass,
                            componentClass))
        }

        var targetClass: Class<*>? = target.javaClass
        var method: Method? = methods[targetClass]
        while (method == null && targetClass != null) {
            targetClass = targetClass.superclass
            method = methods[targetClass]
        }

        if (method == null || method.name.contentEquals("equals")) {
            throw RuntimeException(
                    String.format("No %s injecting method exists in %s component", target.javaClass,
                            componentClass))
        }

        try {
            method.invoke(component, target)
        } catch (e: Exception) {
            throw RuntimeException(e)
        }

    }

    companion object {

        private val cache = ConcurrentHashMap<Class<*>, HashMap<Class<*>, Method>>()

        private fun getMethods(componentClass: Class<*>): HashMap<Class<*>, Method>? {
            var methods: HashMap<Class<*>, Method>? = cache[componentClass]
            if (methods == null) {
                synchronized(cache) {
                    methods = cache[componentClass]
                    if (methods == null) {
                        methods = HashMap<Class<*>, Method>()
                        for (method in componentClass.methods) {
                            val params = method.parameterTypes
                            if (params.size == 1) {
                                methods!!.put(params[0], method)
                            }
                        }
                        cache.put(componentClass, methods!!)
                    }
                }
            }

            return methods
        }
    }
}