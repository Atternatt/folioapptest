package com.m2f.folioapp.di

import android.app.Application
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * @author Marc Moreno
 * @version 1
 */
@Module
class ApplicationModule(val application: Application) {


    @Provides
    @Singleton
    fun providesApplication(): Application {
        return application
    }
}