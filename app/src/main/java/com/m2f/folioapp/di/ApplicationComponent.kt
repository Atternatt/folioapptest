package com.m2f.folioapp.di

import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * @author Marc Moreno
 * @version 1
 *
 * this is the core component that suplies the core dependencies of the application
 */
@Singleton
@Component(modules = arrayOf(ApplicationModule::class, NetworkModule::class))
interface ApplicationComponent {

    fun providesRetrofit():Retrofit

}