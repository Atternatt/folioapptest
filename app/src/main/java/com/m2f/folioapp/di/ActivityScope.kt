package com.m2f.folioapp.di

import kotlin.annotation.Retention
import javax.inject.Scope

/**
 * this scope is for Activity related lifecycles
 */
@Scope
@Retention(AnnotationRetention.RUNTIME)
annotation class ActivityScope