package com.m2f.folioapp.di

/**
 * @author Marc Moreno
 * @version 1
 *
 * This defines a injector object
 */
interface Injector {

    fun inject(component: Any)
}