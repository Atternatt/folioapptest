package com.m2f.folioapp.data.feature.bath

import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.Result
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.Single
import rx.observers.TestSubscriber

/**
 * @author Marc Moreno
 * *
 * @version 1
 */
class ApiImplTest {

    lateinit var api: Api

    @Before fun setUp() {
        val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl("http://www.janes.cat/ignis/")
                .build()

        api = ApiImpl(retrofit)
    }

    @Test fun getWaterInfo() {
        val response: Single<Result<WaterInfoDTO>> = api.getWaterInfo()
        val testSubscriber: TestSubscriber<Result<WaterInfoDTO>> = TestSubscriber()

        response.subscribe(testSubscriber)

        testSubscriber.awaitTerminalEvent()
        testSubscriber.assertNoErrors()
        testSubscriber.assertTerminalEvent()

        val onNextEvents = testSubscriber.onNextEvents

        assert(onNextEvents != null && onNextEvents.size == 1)

    }

}