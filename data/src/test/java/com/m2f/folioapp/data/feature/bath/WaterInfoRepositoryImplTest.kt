package com.m2f.folioapp.data.feature.bath

import com.m2f.folioapp.feature.bath.WaterInfo
import com.m2f.folioapp.feature.bath.WaterInfoRepository
import org.junit.Before
import org.junit.Test
import retrofit2.Retrofit
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import rx.observers.TestSubscriber

/**
 * @author Marc Moreno
 * *
 * @version 1
 */
class WaterInfoRepositoryImplTest {

    lateinit var api: Api
    lateinit var repository: WaterInfoRepository

    @Before fun setUp() {
        val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .baseUrl("http://www.janes.cat/ignis/")
                .build()

        api = ApiImpl(retrofit)
        repository = WaterInfoRepositoryImpl(api)
    }

    @Test fun getWaterInfo() {

        val waterInfo = repository.getWaterInfo()
        val testSubscriber:TestSubscriber<WaterInfo> = TestSubscriber()

        waterInfo.subscribe(testSubscriber)

        testSubscriber.awaitTerminalEvent()
        testSubscriber.assertNoErrors()

        val onNextEvents = testSubscriber.onNextEvents

        assert(onNextEvents != null && onNextEvents.size == 1)
    }
}