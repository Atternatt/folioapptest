package com.m2f.folioapp.data.feature.bath

import retrofit2.adapter.rxjava.Result
import retrofit2.http.GET
import rx.Single

/**
 * @author Marc Moreno
 * @version 1
 */
interface Api {

    @GET("waterinfo")
    fun getWaterInfo():Single<Result<WaterInfoDTO>>
}