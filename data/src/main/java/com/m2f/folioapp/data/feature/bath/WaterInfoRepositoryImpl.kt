package com.m2f.folioapp.data.feature.bath

import com.m2f.folioapp.feature.bath.WaterInfo
import com.m2f.folioapp.feature.bath.WaterInfoRepository
import rx.Single
import javax.inject.Inject

/**
 * @author Marc Moreno
 * @version 1
 */
class WaterInfoRepositoryImpl @Inject constructor(val api: Api): WaterInfoRepository {

    override fun getWaterInfo(): Single<WaterInfo> {
        return api.getWaterInfo()
                .map { result -> result.response() }
                .map { response -> response.body() }
                .map(WaterInfoDTO::toWaterInfo)
    }
}