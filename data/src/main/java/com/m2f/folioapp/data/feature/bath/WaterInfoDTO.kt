package com.m2f.folioapp.data.feature.bath

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class WaterInfoDTO {

    @SerializedName("hot_water")
    @Expose
    var hotWater: Float = 0f
    @SerializedName("cold_water")
    @Expose
    var coldWater: Float = 0f
    @SerializedName("hot_water_lpm")
    @Expose
    var hotWaterLpm: Float = 0f
    @SerializedName("cold_water_lpm")
    @Expose
    var coldWaterLpm: Float = 0f

}

