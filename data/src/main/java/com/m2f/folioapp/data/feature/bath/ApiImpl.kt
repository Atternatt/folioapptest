package com.m2f.folioapp.data.feature.bath

import retrofit2.Retrofit
import retrofit2.adapter.rxjava.Result
import rx.Single
import rx.schedulers.Schedulers
import javax.inject.Inject

/**
 * @author Marc Moreno
 * @version 1
 */
class ApiImpl @Inject constructor(retrofit: Retrofit): Api{

    val api: Api

    init {
        api = retrofit.create(Api::class.java)
    }


    override fun getWaterInfo(): Single<Result<WaterInfoDTO>> {
        return api.getWaterInfo().subscribeOn(Schedulers.io())
    }
}