package com.m2f.folioapp.data.feature.bath

import com.m2f.folioapp.feature.bath.WaterInfo

/**
 * @author Marc Moreno
 * @version 1
 */

fun WaterInfoDTO.toWaterInfo():WaterInfo {
    return WaterInfo(this.hotWater, this.coldWater, this.hotWaterLpm, this.coldWaterLpm)
}
